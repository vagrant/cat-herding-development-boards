#+TITLE: Cat-Herding Development Boards
#+AUTHOR: Vagrant Cascadian
#+EMAIL: vagrant@debian.org
#+DATE: DebConf19 2019-07-25
#+LANGUAGE:  en
#+OPTIONS:   H:1 num:t toc:nil \n:nil @:t ::t |:t ^:t -:t f:t *:t <:t
#+OPTIONS:   TeX:t LaTeX:t skip:nil d:nil todo:t pri:nil tags:not-in-toc
#+OPTIONS: ^:nil
#+INFOJS_OPT: view:nil toc:nil ltoc:t mouse:underline buttons:0 path:http://orgmode.org/org-info.js
#+EXPORT_SELECT_TAGS: export
#+EXPORT_EXCLUDE_TAGS: noexport
#+startup: beamer
#+LaTeX_CLASS: beamer
#+LaTeX_CLASS_OPTIONS: [bigger]
#+latex_header: \mode<beamer>{\usetheme{Madrid}}
#+LaTeX_CLASS_OPTIONS: [aspectratio=169]
#+BEGIN_comment
2019-07-25 15:00 Auditório

Debian supports a growing number of development boards, mostly running
one of Debian’s several ARM architectures. More boards are released to
the wild every month! Sometimes they go feral, abandoned by their
manufacturers and supported by volunteer communities. A few of the
more recent ones even act suspiciously like laptops. The challenge is
that they are all so similarly different. In order to boot Debian on a
new, unique board, changes may be required in several packages, some
needing coordination upstream, and some packaging changes may be
specific to Debian. I’ll touch on my workflows, communication
channels, observation habits, and such, to share ideas on how you too
can partake in the fun!

Bio: Vagrant Cascadian maintains a variety of firmware packages in
Debian, including u-boot, arm-trusted-firmware and most recently
opensbi. In support of reproducible builds, Vagrant maintains a build
zoo of over 20 boards constantly rebuilding all of Debian. In the
right context, Vagrant enjoys being thrown around even more than
tinkering with firmware.
#+END_comment

* Who am I

** text
    :PROPERTIES:
    :BEAMER_col: 0.5
    :END:

  |             | Debian |
  |-------------+--------+
  | user        |   2001 |
  | contributor |   2004 |
  | maintainer  |   2008 |
  | developer   |   2010 |

** image
    :PROPERTIES:
    :BEAMER_col: 0.4
    :END:

[[./images/vagrantupsidedown.png]]

* What is a Development Board

[[./images/bbx15_20160630_012.jpg]]

* Case study: U-boot in debian

In october 2013

Almost 9 months since the last Debian upload.

18 supported platforms

* u-boot: the gentle ask

https://bugs.debian.org/726699

I've done some work towards updating to the new version and could help with
an upload or two if needed... but probably not long-term maintenance.

* u-boot: your new job!

https://bugs.debian.org/726699

  #+BEGIN_SRC Makefile
  > I've done some work towards updating to the new version and could help with
  > an upload or two if needed... but probably not long-term maintenance.

  There's basically no one willing to do long-term maintenance, so
  have a blast!
  #+END_SRC

* Six years later

Today, 2019:

<blink>89 supported platforms!!!1one!!</blink>

https://salsa.debian.org/debian/u-boot/blob/master/debian/targets

* Get people involved

How many people does it take to maintain 89 boards?

https://salsa.debian.org/debian/u-boot/blob/master/debian/targets

* Get people involved, really

  https://wiki.debian.org/U-boot/Status

* u-boot: upstream

irc.freenode.net: #u-boot

  https://lists.denx.de/pipermail/u-boot/

  https://patchwork.ozlabs.org/project/uboot/list/?q=SOMEBOARD

* The Zoo

The Reproducible Builds Zoo

[[./images/squid-monster-2016-01-26.jpg]]

* linux kernel: upstream

https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git

https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable

* linux kernel: patch hunting

https://git.kernel.org/pub/scm/linux/kernel/git/next/linux-next.git

https://patchwork.kernel.org/project/linux-arm-kernel/list/?q=SOMETHING

* linux kernel: debian

https://salsa.debian.org/kernel-team/linux/

- multiplatform kernels only
- modular

* flash-kernel

https://salsa.debian.org/installer-team/flash-kernel/

- Generates boot scripts
- Copies around .dtb (device-tree-blob)

* debian-installer

https://salsa.debian.org/installer-team/debian-installer

- bootloader
- kernel
- flash-kernel

* debian-installer: the easy bit

  #+BEGIN_SRC Makefile
  diff --git a/build/boot/arm/u-boot-image-config b/build/boot/arm/u-boot-image-config
  index 271fdf270..44bca5b33 100644
  --- a/build/boot/arm/u-boot-image-config
  +++ b/build/boot/arm/u-boot-image-config
  @@ -21,6 +21,7 @@ A20-OLinuXino-Lime /usr/lib/u-boot/A20-OLinuXino-Lime/u-boot-sunxi-with-spl.bin
   A20-OLinuXino-Lime2 /usr/lib/u-boot/A20-OLinuXino-Lime2/u-boot-sunxi-with-spl.bin 16
   A20-OLinuXino-MICRO /usr/lib/u-boot/A20-OLinuXino_MICRO/u-boot-sunxi-with-spl.bin 16
   BananaPi /usr/lib/u-boot/Bananapi/u-boot-sunxi-with-spl.bin 16
  +BananaPiM2Berry /usr/lib/u-boot/bananapi_m2_berry/u-boot-sunxi-with-spl.bin 16
   BananaPro /usr/lib/u-boot/Bananapro/u-boot-sunxi-with-spl.bin 16
   Cubieboard /usr/lib/u-boot/Cubieboard/u-boot-sunxi-with-spl.bin 16
   Cubieboard2 /usr/lib/u-boot/Cubieboard2/u-boot-sunxi-with-spl.bin 16
  #+END_SRC

* debian-installer: the hard bit

Kernel modules in .udeb

crc-modules crypto-dm-modules crypto-modules fb-modules i2c-modules
input-modules md-modules mmc-modules mtd-core-modules nic-modules
nic-shared-modules nic-usb-modules nic-wireless-modules sata-modules
scsi-core-modules scsi-modules scsi-nic-modules usb-modules
usb-storage-modules

* debian-installer: the quick bits

https://d-i.debian.org/daily-images/armhf/daily/netboot/SD-card-images/

zcat firmware.none.img.gz  partition.img.gz > test.img

dd if=/usr/lib/u-boot/NEWBOARD/u-boot.img of=test.img seek=N

sudo mount -o loop test.img /mnt

* debian-installer: my favorite workaround

  #+BEGIN_SRC shell
apt download linux-image-VERSION-armmp

dpkg-deb -x linux-image-VERSION-armmp .

find lib/ | cpio --quiet -o -H newc | gzip > cpio.modules.gz

cat initrd.gz cpio.modules.gz > initrd-with-all-the-modules.gz

cp initrd-with-all-the-modules.gz /mnt/initrd.gz
  #+END_SRC

* Working on a pinebook

[[./images/Pinebook_14_Inch_with_Ethernet_Adapter.jpg]]

* arm-trusted-firmware

https://salsa.debian.org/debian/arm-trusted-firmware

- Nearly all 64-bit ARM platforms need it
- Upstream recently migrated to trustedfirmware.org

* Getting people involved: IRC

irc.freenode.net

#u-boot

#linux-sunxi

#linux-rockchip

#linux-amlogic

irc.oftc.net

#debian-arm

* Thanks

- Debian
- #debian-arm
- #u-boot
- #debian-kernel
- #debian-boot
- #linux-sunxi
- And so many individuals others...

* More quick hacks!
* Only the basic kernel

  #+BEGIN_SRC Makefile
    [amd64,arm64,i386] Disable rt featureset.

diff --git a/debian/config/amd64/defines b/debian/config/amd64/defines
index 6ab7192fdb00..b61fd089f7dc 100644
--- a/debian/config/amd64/defines
+++ b/debian/config/amd64/defines
@@ -1,7 +1,6 @@
 [base]
 featuresets:
  none
- rt
 kernel-arch: x86

 [build]
  #+END_SRC

* Sign like you have no access to the kernel signing keys

  #+BEGIN_SRC Makefile
    [amd64,arm64,i386] Disable signed-code.

diff --git a/debian/config/amd64/defines b/debian/config/amd64/defines
index 75705b2a8af3..6ab7192fdb00 100644
--- a/debian/config/amd64/defines
+++ b/debian/config/amd64/defines
@@ -7,7 +7,7 @@ kernel-arch: x86
 [build]
 debug-info: true
 image-file: arch/x86/boot/bzImage
-signed-code: true
+signed-code: false
 vdso: true

 [image]
  #+END_SRC

* Cross building kernel timesavers

  #+BEGIN_SRC Makefile
DEB_KERNEL_DISABLE_DEBUG=yes sbuild \
--profiles='pkg.linux.notools nodoc nopython
    pkg.linux.udeb-unsigned-test-build cross' \
--no-arch-all \
--host=arm64
  #+END_SRC

* Copyright

  Copyright 2019 Vagrant Cascadian <vagrant@debian.org>

  This work is licensed under the Creative Commons
  Attribution-ShareAlike 4.0 International License.

  To view a copy of this license, visit
  https://creativecommons.org/licenses/by-sa/4.0/
